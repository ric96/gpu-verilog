`timescale 1ns / 1ps

module top(
    
    input clk,
    // Input from reset button (active low)
    input rst_n,

    // VGA connections
    output reg [3:0] pixelR,
    output reg [3:0] pixelG,
    output reg [3:0] pixelB,
    output hsync_out,
    output vsync_out
    );

wire clk_25;
wire rst = ~rst_n; // make reset active high
wire inDisplayArea;
wire [9:0] CounterX;
wire [9:0] CounterY;
wire [3:0] selector;
reg [19:0] rando = 0, w_addr;
reg [7:0]  w_data=0, r_addr=0;
wire [7:0] ROMinst;
reg en_write;
wire [0:63] font_byte;
reg [5:0] font_count = 0;
reg [25:0] i = 0;

reg [7:0] color_map_addr;
wire [11:0] color_map_dat;

clk_25MHz clk_video(
  .CLK_IN1(clk),
  .CLK_OUT1(clk_25),
  .RESET(rst)
);

hvsync_generator hvsync(
  .clk(clk_25),
  .vga_h_sync(hsync_out),
  .vga_v_sync(vsync_out),
  .CounterX(CounterX),
  .CounterY(CounterY),
  .inDisplayArea(inDisplayArea)
);
    
 sram vram (
        .i_clk(clk), 
        .r_addr(rando),
        .w_addr(w_addr), 
        .en_write(en_write), 
        .w_data(w_data),
        .r_data(ROMinst)
);

srom font_rom (
        .i_clk(clk), 
        .r_addr(r_addr),
        .r_data(font_byte)
);

crom color_map_rom (
        .i_clk(clk_25), 
        .r_addr(color_map_addr),
        .r_data(color_map_dat)
);



always @(posedge clk_25)
begin
  if (inDisplayArea) begin
      
       if (rando < 307200) begin
        rando = ((640*CounterY)+CounterX);
        if (ROMinst > 0) begin
            pixelR[1:0] <= ROMinst[5:5];
            pixelR[2:2] <= ROMinst[6:6];
            pixelR[3:3] <= ROMinst[7:7];
            pixelG[1:0] <= ROMinst[2:2];
            pixelG[2:2] <= ROMinst[3:3];
            pixelG[3:3] <= ROMinst[4:4];
            pixelB[1:0] <= ROMinst[0:0];
            pixelB[3:2] <= ROMinst[1:1];
        end 
        if (ROMinst == 0) begin
            pixelR[3:0] <= 4'b0000;
            pixelG[3:0] <= 4'b0000;
            pixelB[3:0] <= 4'b0000;
        end
            
       end
       else begin
            rando = 0;
       end
    
  end
  else begin// if it's not to display, go dark
      pixelR <= 4'b0000;
      pixelG <= 4'b0000;
      pixelB <= 4'b0000;
  end
end

/**
color_map_addr <= ROMinst;
        pixelR <= color_map_dat[3:0];
        pixelG <= color_map_dat[7:4];
        pixelB <= color_map_dat[11:8];
**/

always @(posedge clk) begin
    en_write <= 1;
    
    font_count <= font_count + 1;
    
    //font_byte[0:63] <= font_byte_wire;
    
    w_addr <= ((640*(font_count/8)) + (font_count%8));
    if(font_byte[font_count])
        w_data[7:0] <= 8'hff;
    else
        w_data[7:0] <= 8'h00;    
    //w_data <= w_data + 1;
    i = i + 1;
end

always @(posedge i[25:25]) begin
    r_addr <= r_addr + 1;
end


endmodule


module sram #(parameter ADDR_WIDTH = 20, DATA_WIDTH = 8, DEPTH = 307200) (
    input wire i_clk,
    input wire [ADDR_WIDTH-1:0] r_addr,
    input wire [ADDR_WIDTH-1:0] w_addr, 
    input wire en_write,
    input wire [DATA_WIDTH-1:0] w_data,
    output reg [DATA_WIDTH-1:0] r_data 
    );

    reg [DATA_WIDTH-1:0] memory_array [0:DEPTH-1]; 
    
    initial begin
        $readmemh("image.mem", memory_array, 0, DEPTH-1);
    end

    always @ (posedge i_clk)
    begin
        if(en_write) begin
            memory_array[w_addr] <= w_data;
        end
        
        r_data <= memory_array[r_addr];
            
    end
endmodule

module srom #(parameter ADDR_WIDTH = 8, DATA_WIDTH = 64, DEPTH = 256) (
    input wire i_clk,
    input wire [ADDR_WIDTH-1:0] r_addr,
    output reg [DATA_WIDTH-1:0] r_data 
    );

    reg [DATA_WIDTH-1:0] memory_array [0:DEPTH-1]; 

    initial begin
        $readmemh("font_rom.mem", memory_array, 0, DEPTH-1);
    end

    always @ (posedge i_clk)
    begin
        
        r_data <= memory_array[r_addr];
            
    end
endmodule

module crom #(parameter ADDR_WIDTH = 8, DATA_WIDTH = 12, DEPTH = 256) (
    input wire i_clk,
    input wire [ADDR_WIDTH-1:0] r_addr,
    output reg [DATA_WIDTH-1:0] r_data 
    );

    reg [DATA_WIDTH-1:0] memory_array [0:DEPTH-1]; 

    initial begin
        $readmemh("color_map.mem", memory_array, 0, DEPTH-1);
    end

    always @ (posedge i_clk)
    begin
        
        r_data <= memory_array[r_addr];
            
    end
endmodule

