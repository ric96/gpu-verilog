`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08/13/2020 12:34:59 AM
// Design Name: 
// Module Name: clk_25MHz
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module clk_25MHz(
    input         CLK_IN1,
    output        CLK_OUT1,
    input         RESET
    );
    
    reg [1:0] r_reg;
    wire [1:0] r_nxt;
    reg clk_track;
    reg [2:0] run = 0;
     
    always @(posedge CLK_IN1 or posedge RESET) begin
          if (RESET)
     begin
        r_reg <= 3'b0;
	clk_track <= 1'b0;
     end
 
  else if (r_nxt == 2'b10)
 	   begin
	     r_reg <= 0;
	     clk_track <= ~clk_track;
	   end
 
  else 
      r_reg <= r_nxt;
   end
     
     assign r_nxt = r_reg+1;   	      
     assign CLK_OUT1 = clk_track;
endmodule
